const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/HomePage.vue'),
        name: 'Home',
      },
      {
        path: '/about',
        component: () => import('pages/AboutPage.vue'),
        name: 'About 3Droid',
      },
      {
        path: '/3DVisualizer',
        component: () => import('pages/3DVisualizerPage.vue'),
        name: '3D Visualizer',
      },
      {
        path: '/auth',
        component: () => import('pages/AuthPage.vue'),
        name: 'Login - Authentification',
      },
      {
        path: '/thingiverse/popular/',
        component: () => import('pages/thingiverse/Popular.vue'),
        name: 'Thingiverse - Popular',
      },
      {
        path: '/thingiverse/search/',
        component: () => import('pages/thingiverse/Search.vue'),
        name: 'Thingiverse - Search',
      },
      {
        path: '/thingiverse/likes/',
        component: () => import('pages/thingiverse/Likes.vue'),
        name: 'Thingiverse - My Likes',
      },
      {
        path: '/thingiverse/collections/',
        component: () => import('pages/thingiverse/Collections.vue'),
        name: 'Thingiverse - My Collections',
      },
      {
        path: '/thingiverse/thing/:id',
        component: () => import('pages/thingiverse/Thing.vue'),
        name: 'Thingiverse - Thing',
      },
      {
        path: '/thingiverse/collection/:id',
        component: () => import('pages/thingiverse/Collection.vue'),
        name: 'Thingiverse - Collection',
      },
      {
        path: '/octoprint/status/',
        component: () => import('pages/octoprint/Status.vue'),
        name: 'Octoprint - Status',
      },
      {
        path: '/octoprint/control/',
        component: () => import('pages/octoprint/Control.vue'),
        name: 'Octoprint - Control',
      },
      {
        path: '/octoprint/files/',
        component: () => import('pages/octoprint/FileList.vue'),
        name: 'Octoprint - FileList',
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue'),
  },
];

export default routes;
