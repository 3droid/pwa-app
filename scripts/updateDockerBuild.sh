#!/usr/bin/env bash

# This variable is the only variable to change
REGISTRY_GROUP_URL="registry.gitlab.com/3droid/"

# Files to watch for Dockerfile-Base
WATCH_LIST_BASE=("scripts/updateDockerBase.sh" \
		     "docker/base.Dockerfile" \
		     "package.json" \
		     "package-lock.json")

# Files to watch for Dockerfile Project
WATCH_LIST=("scripts/updateDockerBuild.sh" \
		"docker" \
		"/tmp/md5-base" \
		"src" \
		"src-pwa" \
		"public" \
		"quasar.conf.js" \
		"babel.config.js" \
		"jsconfig.json" \
		".eslintrc.js" \
		".postcssrc.js")

# This function initializes the system prior to main processing
init () {
    set -o nounset    # Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
    ulimit -c 0       # Set Core size to 0

    importDotEnv
    # Call the function cleanupBeforeExit when the signal EXIT is catched
    trap "cleanupBeforeExit" EXIT
}

# This function sources the .env file.
importDotEnv () {
    if [ -f .env ]; then
	export $(sed 's/[[:blank:]]//g; /^#/d' .env | xargs)
    fi
}

# This function checks if docker is running on the system, if not, it exits the script.
checkDockerRunning () {
    docker info > /dev/null
    if [ $? == 1 ]; then
	logwarning "Docker is not running, exiting the script now."
	exit 1
    fi
}

# This function is called before exiting the program
cleanupBeforeExit () {
    if [ -f /tmp/md5-base ]; then
        rm /tmp/md5-base
    fi
    loginfo "updateDockerBuild Done"
}

# Format strings for logfunctions
format () {
    echo "$(date +"%d/%m/%y %T") [${1}]";
}

loginfo ()  { echo -e "\033[32m$(format INFO)\033[0m ${@}"; }
lognotice ()  { echo -e "\033[34m$(format NOTICE)\033[0m ${@}"; }
logwarning () { echo -e "\033[33m$(format WARNING)\033[0m ${@}"; }
logerror () { echo -e "\033[31m$(format ERROR)\033[0m ${@}" 1>&2; }

# Overall processing
main ()
{
    init "$@"
    BRANCH=${CI_COMMIT_BRANCH:-"local"}
    BRANCH=`echo ${1:-${BRANCH}} | tr '[:upper:]' '[:lower:]'`
    ACTU_PATH=`pwd | tr [:upper:] [:lower:]`
    FOLDER_NAME=${ACTU_PATH##*/}
    checkDockerRunning
    if [[ `uname -s` == "Darwin" ]]; then
        # Mac OSX
        MD5_TOOL=md5
        AWK_HASH='4'
    else
        MD5_TOOL=md5sum
        AWK_HASH='1'
    fi

    # Hash md5 de 'updateDockerBase.sh base.Dockerfile and package.json'
    FOLDER_CODE_HASH_BASE="$(find ${WATCH_LIST_BASE[*]} -type f -exec ${MD5_TOOL} {} \; \
    	      | awk -v NB="$AWK_HASH" "{print $"NB"}" > /tmp/md5-base && ${MD5_TOOL} /tmp/md5-base \
			  | awk -v NB="$AWK_HASH" "{print $"NB"}")" 

    # Hash md5 de 'updateDockerBuild.sh Dockerfiles md5-base src/ public/'
    FOLDER_CODE_HASH="$(find ${WATCH_LIST[*]} -type f -exec ${MD5_TOOL} {} \; \
			  | awk -v NB="$AWK_HASH" "{print $"NB"}" > /tmp/md5 && ${MD5_TOOL} /tmp/md5 \
			  | awk -v NB="$AWK_HASH" "{print $"NB"}" && rm /tmp/md5-base /tmp/md5)" 

    REGISTRY_URL=""
    PROJECT_VERSION=${FOLDER_CODE_HASH_BASE}-${FOLDER_CODE_HASH}
    if [ ${BRANCH} == "master" ]; then
        loginfo "Branch master detected on gitlab, deploying docker image to :" \
                "${CI_REGISTRY_IMAGE:-${REGISTRY_GROUP_URL}} instead of ${REGISTRY_URL}."
        REGISTRY_URL=${CI_REGISTRY_IMAGE:-${REGISTRY_GROUP_URL}${FOLDER_NAME}}
    else
        REGISTRY_URL=${CI_REGISTRY_IMAGE:-${REGISTRY_GROUP_URL}${FOLDER_NAME}}"/"${BRANCH}
    fi
    loginfo "Dockerfile-Base is in ${FOLDER_CODE_HASH_BASE} version, checking if it's up to date."
    bash ./scripts/updateDockerBase.sh
    if [ $? == 1 ]; then
	exit 1;
    fi

    # Check if already authed to registry.gitlab.com
    AUTHED=false
    DOCKER_CONFIG=~/.docker/config.json
    if [ -f "${DOCKER_CONFIG}" ]; then
	cat ${DOCKER_CONFIG} | grep "registry.gitlab.com" > /dev/null
        if [ $? == 0 ]; then
	    loginfo "User seems to be authenticated to registry.gitlab.com"
	    AUTHED=true
	else
	    lognotice "User is not authenticated to registry.gitlab.com"
        fi
    fi

    #Docker login if no auth
    if [ ${AUTHED} == false ]; then
        loginfo "Authenticating to Gitlab Registry."
	if [ ${CI_REGISTRY_USER}"test" == "test" ]; then
	    docker login registry.gitlab.com
        else
	    loginfo "Welcome Gitlab-CI Runner :D"
	    docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY} 2>/dev/null
        fi
    fi

    # Pull the project
    loginfo "Project is in ${PROJECT_VERSION} version, checking if it's up to date."
    docker pull ${REGISTRY_URL}:${PROJECT_VERSION}

    #Build
    if [ $? == 1 ]; then
        lognotice "Hash of Project not found on registry, building a new one on :"
        loginfo "${REGISTRY_URL}:${PROJECT_VERSION}, ${FOLDER_CODE_HASH}, latest."
	if [ ${BRANCH} == "master" ]; then
	    DOCKERFILE="prod.Dockerfile"
	elif [ ${BRANCH} == "develop" ]; then
	    DOCKERFILE="prod.Dockerfile"
	else
	    DOCKERFILE="local.Dockerfile"
	fi
        docker build -f docker/${DOCKERFILE} \
	       --cache-from ${REGISTRY_URL}:latest \
	       --tag ${REGISTRY_URL}:${PROJECT_VERSION} \
	       --tag ${REGISTRY_URL}:${FOLDER_CODE_HASH} \
	       --tag ${REGISTRY_URL}:latest .
	if [ $? == 0 ]; then
	    loginfo "Pushing new images to gitlab registry."
	    loginfo "Pushing ${REGISTRY_URL}:${PROJECT_VERSION}"
	    docker push ${REGISTRY_URL}:${PROJECT_VERSION}
	    loginfo "Pushing ${REGISTRY_URL}:${FOLDER_CODE_HASH}"
	    docker push ${REGISTRY_URL}:${FOLDER_CODE_HASH}
	    loginfo "Pushing ${REGISTRY_URL}:latest"
	    docker push ${REGISTRY_URL}:latest
	else
	    logwarning "Docker build failed, exiting the script now."
	    exit 1
	fi
    fi
}

# script entry point
main "$@"
