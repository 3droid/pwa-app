# ---- Release based on Dockerfile-Base ----
FROM registry.gitlab.com/3droid/pwa-app/base:latest

# Setting environment to docker by default,
# you can change it at build time with --build-arg
ARG environment=local
ENV APP_ENV=${environment}
ENV APP_SERVICES_ENV=${environment}

WORKDIR /app

# Import app sources
COPY src /app/src
COPY src-pwa /app/src-pwa
COPY public /app/public
COPY quasar.conf.js /app
COPY babel.config.js /app
COPY jsconfig.json /app
COPY .postcssrc.js /app
COPY .eslintrc.js /app
COPY .eslintignore /app

# start app
CMD ["npm", "start"]
