FROM node:lts-alpine

#Set the timezone to Paris
ENV TZ=Europe/Paris

RUN \
    apk update \
    # install curl and bash
    && apk add bash curl \
    # cleanup
    && rm /var/cache/apk/* \
    && rm -rf /var/lib/apk/

WORKDIR /app/

COPY package.json /app
COPY package-lock.json /app

# install node packages
RUN npm set progress=false && \
    npm config set depth 0 && \
    CI=true npm install
