#!/usr/bin/env bash

# This variable is the only variable to change
REGISTRY_GROUP_URL="registry.gitlab.com/3droid/"

# This function initializes the system prior to main processing
init () {
    set -o nounset    # Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
    ulimit -c 0       # Set Core size to 0

    importDotEnv
    # Call the function cleanupBeforeExit when the signal EXIT is catched
    trap "cleanupBeforeExit" EXIT
}

# This function sources the .env file.
importDotEnv () {
    if [ -f .env ]; then
	export $(sed 's/[[:blank:]]//g; /^#/d' .env | xargs)
    else
	lognotice ".env file not found, this file may be necessary if you want to connect to the database."
    fi
}

# This function checks if docker is running on the system, if not, it exits the script.
checkDockerRunning () {
    docker info > /dev/null
    if [ $? == 1 ]; then
	logwarning "Docker is not running, exiting the script now."
	exit 1
    fi
}

# This function is called before exiting the program
cleanupBeforeExit () {
    loginfo "deploy.sh Done"
}

# Format strings for logfunctions
format () {
    echo "$(date +"%d/%m/%y %T") [${1}]";
}

loginfo ()  { echo -e "\033[32m$(format INFO)\033[0m ${@}"; }
lognotice ()  { echo -e "\033[34m$(format NOTICE)\033[0m ${@}"; }
logwarning () { echo -e "\033[33m$(format WARNING)\033[0m ${@}"; }
logerror () { echo -e "\033[31m$(format ERROR)\033[0m ${@}" 1>&2; }

# Overall processing
main ()
{
    init "$@"
    BRANCH=${1:-"local"}
    BRANCH=`echo "${BRANCH}" | tr '[:upper:]' '[:lower:]'`
    ACTU_PATH=`pwd | tr [:upper:] [:lower:]`
    FOLDER_NAME=${ACTU_PATH##*/}
    checkDockerRunning

    DC_FILE=""
    REGISTRY_URL="${REGISTRY_GROUP_URL}${FOLDER_NAME}/${BRANCH}"
    if [ ${BRANCH} == "master" ]; then
	lognotice "Production env detected, using docker-compose.prod.yml as docker-compose file."
        DC_FILE="docker-compose.prod.yml"
	REGISTRY_URL="${REGISTRY_GROUP_URL}${FOLDER_NAME}"
    elif [ ${BRANCH} == "develop" ]; then
	lognotice "Development env detected, using docker-compose.dev.yml as docker-compose file."
        DC_FILE="docker-compose.dev.yml"
    fi
    if [ ${BRANCH} != "jest" ]; then
	loginfo "Trying to pull ${REGISTRY_URL}:latest"
	if [[ ${BRANCH} == "master" || ${BRANCH} == "develop" ]]; then
	    docker-compose -f ${DC_FILE} pull
	else
	    docker pull "${REGISTRY_URL}:latest"
	fi
	if [ $? == 1 ]; then
    	    logerror "Image ${REGISTRY_URL} not found, aborting script."
    	    exit 1
	fi
	loginfo "Stopping and removing the old container ${FOLDER_NAME}."
	docker stop ${FOLDER_NAME}
	if [ $? == 1 ]; then
    	    lognotice "Container ${FOLDER_NAME} is not running."
	else
	    docker rm ${FOLDER_NAME}
	    loginfo "Waiting 5 seconds to be sure the container is removed."
	    sleep 5
	fi
    fi

    if [[ ${BRANCH} == "master" || ${BRANCH} == "develop" ]]; then
	loginfo "Deploying using ${DC_FILE}."
	docker-compose -f ${DC_FILE} up -d
    elif [ ${BRANCH} == "jest" ]; then
	loginfo "Starting a new container named jest-${FOLDER_NAME}."
	docker run --name jest-${FOLDER_NAME} --rm jest-${FOLDER_NAME} 
    elif [ ${BRANCH} == "local" ]; then
	loginfo "Starting a new container named ${FOLDER_NAME} on port 8080."
	if [ -f .env ]; then
	    docker run --name ${FOLDER_NAME}\
		   --env-file=.env -it --rm -d\
		   -v `pwd`/src:/app/src -v `pwd`/public:/app/public\
		   -p 8080:8080 "${REGISTRY_URL}:latest"
	else
	    docker run --name ${FOLDER_NAME}\
		   -it --rm -d\
		   -v `pwd`/src:/app/src -v `pwd`/public:/app/public\
		   -p 8080:8080 "${REGISTRY_URL}:latest"
	fi
    fi
}

# script entry point
main "$@"
