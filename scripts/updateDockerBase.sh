#!/usr/bin/env bash

# This variable is the only variable to change
REGISTRY_GROUP_URL="registry.gitlab.com/3droid/"

# Files to watch for Dockerfile-Base
WATCH_LIST=("scripts/updateDockerBase.sh" \
		"docker/base.Dockerfile" \
		"package.json" \
		"package-lock.json")

# This function initializes the system prior to main processing
init () {
    set -o nounset    # Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
    ulimit -c 0       # Set Core size to 0

    importDotEnv
    # Call the function cleanupBeforeExit when the signal EXIT is catched
    trap "cleanupBeforeExit" EXIT
}

# This function sources the .env file.
importDotEnv () {
    if [ -f .env ]; then
	export $(sed 's/[[:blank:]]//g; /^#/d' .env | xargs)
    fi
}

# This function checks if docker is running on the system, if not, it exits the script.
checkDockerRunning () {
    docker info > /dev/null
    if [ $? == 1 ]; then
	logwarning "Docker is not running, exiting the script now."
	exit 1
    fi
}

# This function is called before exiting the program
cleanupBeforeExit () {
    loginfo "updateDockerBase Done"
}

# Format strings for logfunctions
format () {
    echo "$(date +"%d/%m/%y %T") [${1}]";
}

loginfo ()  { echo -e "\033[32m$(format INFO)\033[0m ${@}"; }
lognotice ()  { echo -e "\033[34m$(format NOTICE)\033[0m ${@}"; }
logwarning () { echo -e "\033[33m$(format WARNING)\033[0m ${@}"; }
logerror () { echo -e "\033[31m$(format ERROR)\033[0m ${@}" 1>&2; }

# Overall processing
main ()
{
    init "$@"
    BRANCH=`echo ${CI_COMMIT_BRANCH:-"local"} | tr '[:upper:]' '[:lower:]'`
    ACTU_PATH=`pwd | tr [:upper:] [:lower:]`
    FOLDER_NAME=${ACTU_PATH##*/}
    REGISTRY_URL=${CI_REGISTRY_IMAGE:-${REGISTRY_GROUP_URL}${FOLDER_NAME}}"/base"
    checkDockerRunning
    if [[ `uname -s` == "Darwin" ]]; then
        # Mac OSX
        MD5_TOOL=md5
        AWK_HASH='4'
    else
        MD5_TOOL=md5sum
        AWK_HASH='1'
    fi

    # Hash md5 de 'updateDockerBase.sh base.Dockerfile package.json package-lock.json'
    FOLDER_CODE_HASH="$(find ${WATCH_LIST[*]} -type f -exec ${MD5_TOOL} {} \; \
        | awk -v NB="$AWK_HASH" "{print $"NB"}" > /tmp/md5-base \
        && ${MD5_TOOL} /tmp/md5-base | awk -v NB="$AWK_HASH" "{print $"NB"}" )"

    # Check if already authed to registry.gitlab.com
    AUTHED=false
    DOCKER_CONFIG=~/.docker/config.json
    if [ -f "${DOCKER_CONFIG}" ]; then
	cat ${DOCKER_CONFIG} | grep "registry.gitlab.com" > /dev/null
        if [ $? == 0 ]; then
	    loginfo "User seems to be authenticated to registry.gitlab.com"
	    AUTHED=true
	else
	    lognotice "User is not authenticated to registry.gitlab.com"
	    exit 1;
        fi
    fi

    if [ ${AUTHED} == false ]; then
        loginfo "Authenticating to Gitlab Registry."
	if [ ${CI_REGISTRY_USER}"test" == "test" ]; then
	    docker login registry.gitlab.com
        else
	    loginfo "Welcome Gitlab-CI Runner :D"
	    docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY} 2>/dev/null
        fi
    fi

    loginfo "Base is in ${FOLDER_CODE_HASH} version, checking if it's up to date."
    loginfo "Checking if the package.json hash tag is on the current image : ${FOLDER_CODE_HASH}."
    docker pull ${REGISTRY_URL}:${FOLDER_CODE_HASH} 2>/dev/null
    if [ $? == 1 ]; then
        lognotice "Hash of Base not found on registry, building a new base on :"
        loginfo "${REGISTRY_URL}:${FOLDER_CODE_HASH}, latest."
        docker build -f docker/base.Dockerfile \
               --cache-from ${REGISTRY_URL}:latest \
               --tag ${REGISTRY_URL}:${FOLDER_CODE_HASH} \
               --tag ${REGISTRY_URL}:latest .
	if [ $? == 0 ]; then
            loginfo "Pushing new images to gitlab registry."
            loginfo "Pushing ${REGISTRY_URL}:${FOLDER_CODE_HASH}"
            docker push ${REGISTRY_URL}:${FOLDER_CODE_HASH}
            loginfo "Pushing ${REGISTRY_URL}:latest"
            docker push ${REGISTRY_URL}:latest
	else
	    logwarning "Docker build of base failed, exiting the script now."
	    exit 1
	fi
    else
        loginfo "The docker image Base is already up to date : ${FOLDER_CODE_HASH}."
        loginfo "Trying to pull latest to make sure it got also the latest tag."
        docker pull ${REGISTRY_URL}:latest 2>/dev/null
    fi
}

# script entry point
main "$@"
