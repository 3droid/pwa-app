# ---- Release based on Dockerfile-Base ----
FROM registry.gitlab.com/3droid/pwa-app/base:latest as build-stage

# Setting environment to docker by default,
# you can change it at build time with --build-arg
ARG environment=production
ENV APP_ENV=${environment}
ENV APP_SERVICES_ENV=${environment}

WORKDIR /app

# Import app sources
COPY src /app/src
COPY src-pwa /app/src-pwa
COPY public /app/public
COPY quasar.conf.js /app
COPY babel.config.js /app
COPY jsconfig.json /app
COPY .postcssrc.js /app
COPY .eslintrc.js /app
COPY .eslintignore /app
COPY .env /app

# Build the frontend for nginx
RUN npm run build:pwa

# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:1.21

ENV NGINX_PORT=80

COPY --from=build-stage /app/dist/pwa/ /usr/share/nginx/html
COPY ./docker/nginx.conf /etc/nginx/conf.d/default.conf
